﻿using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using System.Linq;
using DG.Tweening;
using Random = UnityEngine.Random;
using UnityEngine.Rendering;

public class Actor : MonoBehaviour
{
    public Transform contactPoint;

    protected Rigidbody2D body2D;
    protected Rigidbody body3D;
    protected Collider2D coll2D;
    public List<SpriteRenderer> spriteRenderers;
    public SpriteRenderer markSprite;
    protected Animator animator;
    public Transform spritesRoot;

    protected Dictionary<PropTag, int> affinity;

    public float speed = 0.15f;

    public Suspicion tagState = Suspicion.IsNeutral;

    [Header("clips")]
    public AudioClip[] goodNudges;
    public AudioClip[] dieClips;
    public AudioClip[] LikeClips;
    public AudioClip[] dislikeClips;

    protected GameController controller;

    static int goodNudgeIndex = 0;
    public int nextGoodNudge
    {
        get
        {
            goodNudgeIndex = (goodNudgeIndex + 1) % goodNudges.Length;
            return goodNudgeIndex;
        }
    }

    static int likeClipIndex = 0;
    public int nextLikeClip
    {
        get
        {
            likeClipIndex = (likeClipIndex + 1) % LikeClips.Length;
            return likeClipIndex;
        }
    }

    static int dislikeClipIndex = 0;
    public int nextDislikeClip
    {
        get
        {
            Debug.Log("dislikeClips.Length " + dislikeClips.Length);
            Debug.Log("dislikeClipIndex before " + dislikeClipIndex);
            dislikeClipIndex = (dislikeClipIndex + 1) % dislikeClips.Length;
            Debug.Log("dislikeClipIndex after " + dislikeClipIndex);
            return dislikeClipIndex;
        }
    }

    static int dieClipIndex = 0;
    public int nextDieClip
    {
        get
        {
            dieClipIndex = (dieClipIndex + 1) % dieClips.Length;
            return dieClipIndex;
        }
    }


    protected Tweener Turn;
    protected int EffectPriority;
    public bool isInBooth = false;

    [Space(10)]
    public Corpse CorpsePrefab;

    public void Awake()
    {
        body2D = GetComponentInChildren<Rigidbody2D>();
        body3D = GetComponent<Rigidbody>();
        coll2D = GetComponentInChildren<Collider2D>();
        animator = GetComponent<Animator>();
    }
    public void Init(GameController controller, Spy spy)
    {
        this.controller = controller;
        affinity = new Dictionary<PropTag, int>();
        foreach (var persona in spy.personas)
        {
            affinity = affinity.Union(persona.affinity).ToDictionary(kv => kv.Key, kv => kv.Value);
        }

        var af = spy.personas[Random.Range(0, spy.personas.Count)].affinity;
        var randAff = af.ToList()[Random.Range(0, af.Count)];
        affinity[randAff.Key] *= -1;
        //affinity.Add(randAff.Key, randAff.Value * -1);

        for (int i = 0; i < (int)PropTag.Count; i++)
        {

            if (i != randAff.Value)
                affinity[(PropTag)i] = Random.value > 0.5f ? 1 : -1;

            //if (!affinity.ContainsKey((PropTag)i))
            //    affinity.Add((PropTag)i, Random.value > 0.5f ? 1 : -1);
        }

    }

    public void Start()
    {
        //spriteRenderers[0].color = new Color(Random.value, Random.value, Random.value);
        spriteRenderers[1].color = new Color(Random.value, Random.value, Random.value);
        spriteRenderers[2].color = new Color(Random.value, Random.value, Random.value);
        spriteRenderers[3].color = new Color(Random.value, Random.value, Random.value);
        markSprite.color = Color.clear;

        DecideToMove();
    }

    private Vector3 TrimToBounds(Vector3 vector)
    {
        var bounds = controller.bounds;

        vector = Vector2.Min(controller.bounds.max, vector);
        vector = Vector2.Max(controller.bounds.min, vector);

        return vector;
    }
    bool underEffect = false;
    public void Affect(Vector3 propPosition, PropTag tag, int priority)
    {
        if (priority <= EffectPriority)
            return;

        Vector2 runVector = (propPosition - transform.position);
        Vector2 endPoint = transform.position;
        if (tag == PropTag.Corpse || affinity[tag] < 0)
        {
            endPoint += runVector - runVector.normalized * 3;
            if (Random.value < 0.2f)
            {
                AudioManager.Instance.PlaySFX(dislikeClips[nextDislikeClip], 0.2f);
            }
        }
        else if (affinity[tag] > 0)
        {
            if (Random.value < 0.2f)
            {
                AudioManager.Instance.PlaySFX(LikeClips[nextLikeClip], 0.2f);
            }
            if (runVector.magnitude < 1)
                return;
            endPoint += runVector * 0.7f;
        }
        else
        {
            return;
        }

        EffectPriority = priority;

        TrimToBounds(endPoint);
        underEffect = true;

        StopAllCoroutines();

        StartCoroutine(MoveTo(endPoint, speed * 2, 0, () =>
          {
              EffectPriority = 0;
              DecideToMove();
              underEffect = false;
          }));

    }

    Vector3 debugEndPoint;

    IEnumerator MoveTo(Vector2 endPoint, float _speed, float delay = 0, Action OnComplete = null)
    {
        debugEndPoint = endPoint;
        yield return new WaitForSeconds(delay);
        body3D.velocity = (endPoint - (Vector2)transform.position).normalized * _speed;

        animator.SetBool("Moving", true);
        spritesRoot.localScale = new Vector3(body3D.velocity.x > 0 ? 1 : -1, 1, 1);
        yield return new WaitUntil(() => Vector2.Distance(endPoint, transform.position) < 0.15f);
        body3D.velocity = Vector2.zero;
        animator.SetBool("Moving", false);
        OnComplete.Invoke();
    }


    public void DecideToMove()
    {
        if (EffectPriority == 0 && !controller.hasGameOver)
        {
            if (Random.value < 0.2f)
            {
                animator.SetBool("Moving", false);
                this.RunDelayed(() => DecideToMove(), Random.Range(1, 3));
                return;
            }

            if (Random.value < 0.2f)
            {
                MoveToBooth();
                return;
            }

            var endPoint = (Vector2)transform.position + Random.insideUnitCircle * 2f;

            endPoint = TrimToBounds(endPoint);

            StartCoroutine(MoveTo(endPoint, speed, Random.Range(3, 6), () =>
            {
                DecideToMove();
            }));

        }

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = underEffect ? Color.red : Color.cyan;
        Gizmos.DrawLine(transform.position, debugEndPoint);
    }

    private void Update()
    {
        GetComponent<SortingGroup>().sortingOrder = 1000 - (int)(contactPoint.position.y * 100);
    }

    private void MoveToBooth()
    {
        var boothPositions = controller.boothPositions;
        Transform targetBoothPosition = boothPositions[Random.Range(0, boothPositions.Count)];

        var endPoint = targetBoothPosition.position;

        endPoint = TrimToBounds(endPoint);


        StartCoroutine(MoveTo(endPoint, speed * Random.Range(1.5f, 3.5f), Random.Range(.5f, 3), () =>
        {
            //animator.SetBool("InBooth", true);  //ja j ajja ajq
            animator.SetBool("Moving", false);

            var targetBooth = targetBoothPosition.GetComponent<Booth>();
            targetBooth.ActorEnter(this);
            StartCoroutine(MoveTo(endPoint, speed, Random.Range(.5f, 3), () =>
            {
                if (targetBooth.ActorInBooth == this)
                    targetBooth.ActorLeave();

                animator.SetBool("Moving", true);
                //animator.SetBool("InBooth", false);  //ja ja

                DecideToMove();
            }));
        }));


        //Turn = transform
        //    .DOMove(endPoint, Random.Range(3, 6))
        //    .SetEase(Ease.Linear)
        //    .SetDelay(Random.Range(.5f, 3))
        //    .OnComplete(() =>
        //    {
        //        animator.SetTrigger("Phone");

        //        var targetBooth = targetBoothPosition.GetComponent<Booth>();
        //        targetBooth.ActorEnter(this);
        //        targetBooth.transform
        //            .DOMove(targetBooth.transform.position, Random.Range(.5f, 2))
        //            .OnComplete(() =>
        //            {
        //                if (targetBooth.ActorInBooth == this)
        //                    targetBooth.ActorLeave();

        //                animator.SetTrigger("Stop");
        //            });
        //    });
    }

    public void Die()
    {
        Debug.Log("AAAARRRRRRGHGHGHG");
        AudioManager.Instance.RunDelayed(() =>
        {
            AudioManager.Instance.PlaySFX(dieClips[nextDieClip], 0.2f);
        }, 0.6f);
        var corpse = Instantiate(CorpsePrefab, transform, false);
        corpse.transform.localPosition = Vector3.zero;

        if (this is SpyActor)
        {
            Debug.Log("OH NO! T'WAS ME ALL ALONG!!!!");
            GameOver(true);
        }
        else
        {
            controller.deathCount++;
            controller.AudioIntensifier();
            controller.UpdateCasualtiesPanel();
            if (controller.deathCount >= 3)
            {
                GameOver();
            }
        }

        animator.SetTrigger("Die");
        body3D.velocity = Vector2.zero;
        enabled = false;
        body3D.detectCollisions = false;
        coll2D.enabled = false;

        Destroy(this);
    }

    public void GameOver(bool win = false)
    {
        Debug.Log(win ? "WIN" : "LOSE");
        FindObjectOfType<GameController>().GameOver(win);
    }

    public void SetSuspectLabel(Suspicion suspicion)
    {
        tagState = tagState == suspicion ? Suspicion.IsNeutral : suspicion;
        markSprite.color = tagState == Suspicion.IsBadGuy ? new Color(1, .15f, .25f) : tagState == Suspicion.IsGoodGuy ? new Color(.15f, .4f, 1f) : Color.clear;

        if (Random.value < 0.2f)
        {
            AudioManager.Instance.PlaySFX(goodNudges[nextGoodNudge], 0.2f);
        }
    }

    public enum Suspicion
    {
        IsBadGuy = -1,
        IsNeutral = 0,
        IsGoodGuy = 1
    }
}
