﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Agent : MonoBehaviour, ICollisionListener
{
    public PropTag PropTag;

    private Rigidbody2D body2D;
    private Collider2D coll2D;
    private ColliderHelper helper;
    private SpriteRenderer spriteRenderer;
    private Level level;
    public float radius;

    public int Priority = 1;
    private Coroutine ResetPriorityCoroutine;
    protected Tweener Turn;

    void Awake()
    {
        level = FindObjectOfType<Level>();
        body2D = GetComponentInChildren<Rigidbody2D>();
        coll2D = GetComponentInChildren<Collider2D>();
        ((CircleCollider2D)coll2D).radius = radius;
        helper = GetComponentInChildren<ColliderHelper>();
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
    }

    void Start()
    {
        Debug.Log(helper);
        helper.SetListener(this);
        Loop();
    }

    void Loop()
    {
        var thing = level.propsPositions[Random.Range(0, level.propsPositions.Count)];
        Debug.Log("Walking towartds " + thing, thing.gameObject);
        var dist = (thing.position - transform.position);
        var relativeTarget = (dist.magnitude - 1.6f) * dist.normalized;

        Turn = transform.DOMove(relativeTarget, 0.5f)
            .SetRelative()
            .SetSpeedBased()
            .SetEase(Ease.Linear)
            .SetDelay(Random.Range(.5f, 3))
            .OnComplete(() =>
            {
                Loop();
            });

    }


    #region trigger events
    public void _OnTriggerEnter(Collider2D other)
    {
    }

    public void _OnTriggerExit(Collider2D other)
    {
    }

    public void _OnTriggerStay(Collider2D other)
    {
        var actor = other.GetComponentInParent<Actor>();
        if (actor != null)
        {
            actor.Affect(transform.position, PropTag, Priority);
        }
    }
    #endregion

    private IEnumerator ResetPriority()
    {
        yield return new WaitForSeconds(.4f);
        Priority = 1;
    }
}
