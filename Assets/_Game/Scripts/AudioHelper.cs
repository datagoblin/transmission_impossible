﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioHelper : MonoBehaviour
{
    public AudioClip clip;
    private AudioSource source;
    public float volume = 1f;
    private void Start()
    {
        source = GetComponent<AudioSource>();
    }
    public void OnClick()
    {
        source.PlayClip(clip, false, volume);
    }

}
