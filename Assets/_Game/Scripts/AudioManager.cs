﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class AudioManager : MonoBehaviour
{

    public static AudioManager Instance;
    public List<AudioSource> BGMSources;
    public List<AudioSource> SFXSources;

    private void Awake()
    {

        Instance = this;

        BGMSources = new List<AudioSource>();
        for (int i = 0; i < 6; i++)
        {
            BGMSources.Add(gameObject.AddComponent<AudioSource>());
        }

        SFXSources = new List<AudioSource>();
        for (int i = 0; i < 10; i++)
        {
            SFXSources.Add(gameObject.AddComponent<AudioSource>());
        }
    }

    public void PlaySFX(AudioClip clip, float volume = 1, float pitch = 1)
    {
        var source = SFXSources.First((s) => !s.isPlaying);
        if (source != null)
        {
            source.PlayClip(clip);
        }
        else
        {
            source = gameObject.AddComponent<AudioSource>();
            SFXSources.Add(source);
            source.PlayClip(clip);
        }


    }
}

public static class Extensions
{
    public static void PlayClip(this AudioSource source, AudioClip clip, bool loop = false, float volume = 1, float pitch = 1)
    {
        if (!source)
            return;

        source.clip = clip;
        source.volume = volume;
        source.pitch = pitch;
        source.loop = loop;
        source.Play();
    }
}
