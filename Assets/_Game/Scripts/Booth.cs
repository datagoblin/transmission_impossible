﻿using UnityEngine;
using UnityEngine.Rendering;

public class Booth : MonoBehaviour
{
    public AudioClip doorSfx;
    public Transform contactPoint;

    public Actor ActorInBooth;
    private Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        GetComponent<SortingGroup>().sortingOrder = 1000 - (int)(contactPoint.position.y * 100);
    }

    public void ActorEnter(Actor actor)
    {
        AudioManager.Instance.PlaySFX(doorSfx);
        ActorLeave();

        animator.SetTrigger("Enter");
        ActorInBooth = actor;
        ActorInBooth.isInBooth = true;
    }

    public void ActorLeave()
    {
        animator.SetTrigger("Leave");
        if (ActorInBooth != null)
        {
            ActorInBooth.gameObject.SetActive(true);

            if (ActorInBooth is SpyActor)
            {
                Debug.Log("SPY CALL!!");
                ((SpyActor)ActorInBooth).SendPacket();
            }
            else
            {
                Debug.Log("normal call");
            }

            ActorInBooth = null;
        }
    }

    public void GetShot()
    {
        if (ActorInBooth != null)
        {
            ActorInBooth.Die();
            ActorInBooth = null;
        }
    }
}
