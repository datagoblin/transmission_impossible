﻿using UnityEngine;

public class ColliderHelper : MonoBehaviour
{


    ICollisionListener _listener;
    public void SetListener(ICollisionListener listener)
    {
        _listener = listener;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (_listener != null)
            _listener._OnTriggerEnter(collision);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (_listener != null)
            _listener._OnTriggerExit(collision);
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (_listener != null)
            _listener._OnTriggerStay(other);
    }

    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    _listener._OnCollisionEnter(collision);
    //}

    //private void OnCollisionExit2D(Collision2D collision)
    //{
    //    _listener._OnCollisionExit(collision);
    //}
}

public interface ICollisionListener
{
    void _OnTriggerEnter(Collider2D other);
    void _OnTriggerExit(Collider2D other);
    void _OnTriggerStay(Collider2D collision);
    //void _OnCollisionEnter(Collision2D collision);
    //void _OnCollisionExit(Collision2D collision);
}
