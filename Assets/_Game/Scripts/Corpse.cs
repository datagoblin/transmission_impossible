﻿using UnityEngine;

public class Corpse : MonoBehaviour, ICollisionListener
{
    private ColliderHelper helper;

    public void _OnTriggerEnter(Collider2D other)
    {
    }

    public void _OnTriggerExit(Collider2D other)
    {
    }

    public void _OnTriggerStay(Collider2D other)
    {
        var actor = other.GetComponentInParent<Actor>();
        if (actor != null && GetComponentInParent<Actor>() != actor)
        {
            actor.Affect(transform.position, PropTag.Corpse, 10);
        }
    }

    void Start()
    {
        helper = GetComponentInChildren<ColliderHelper>();
        helper.SetListener(this);

        this.RunDelayed(() => Destroy(gameObject), 2);
    }
}
