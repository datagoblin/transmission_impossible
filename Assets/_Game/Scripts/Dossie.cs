﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Dossie : MonoBehaviour
{
    public Spy spy;
    public Image backbrop;
    public RectTransform panelRoot;
    public Text nameText;
    public Text missionTitleText;
    public Text missionDescriptionText;
    public Image mugshotImage;
    public List<Sprite> mugshotSprites;
    public List<Text> personaTexts;

    private bool opened = false;


    public void Init(Spy spy)
    {
        nameText.text = spy.fullName;
        mugshotImage.sprite = mugshotSprites[Random.Range(0, mugshotSprites.Count)];
        Debug.Log("spy.mission.description");
        missionTitleText.text = spy.mission.title.ToUpper();
        missionDescriptionText.text = spy.mission.description;

        for (int i = 0; i < spy.personas.Count; i++)
        {
            if (i > spy.personas.Count)
            {
                personaTexts[i].text = "";
                return;
            }

            var desc = spy.personas[i].description;
            personaTexts[i].text = "\"" + desc + "\"";
        }
    }

    public void Show()
    {
        backbrop.raycastTarget = true;
        backbrop.DOFade(0.25f, 0.3f).SetEase(Ease.InOutQuad).OnComplete(() =>
        {
            panelRoot.DOAnchorPosY(-54.38f, 0.5f).SetEase(Ease.InOutQuad);
        });
    }

    public void Hide()
    {

        AudioManager.Instance.PlaySFX(FindObjectOfType<GameController>().sfxCheck);
        panelRoot.DOAnchorPosY(-1020f, 0.5f).SetEase(Ease.InOutQuad).OnComplete(() =>
        {
            backbrop.DOFade(0, 0.3f).SetEase(Ease.InOutQuad);
            backbrop.raycastTarget = false;
        });

    }

}

public class Mission
{
    public string title = "";
    public string description = "";
    public string success = "";
    public string fail = "";

    public static List<Mission> missions = new List<Mission>();

    public Mission(string title, string description, string success, string fail)
    {
        this.title = title;
        this.description = description;
        this.success = success;
        this.fail = fail;
    }

    public static void Init()
    {
        if (missions.Count > 0) return;

        missions.Add(new Mission(
            "President Nudes",
            "This spy have some president’s nudes to spread worldwide.",
            "Good job! You prevent the World War Three! The nudes are safe.",
            "Oh no! The nudes trended worldwide as #NotThatBigDeal and the World War Three has began!"));

        missions.Add(new Mission(
            "Memes Prohibition",
            "This spy have a world declaration that forbids memes forever.",
            "Great! You protected all the memes around the world!",
            "Oh no! All the memes were forbidden, and the 21st century’s communication vehicles are dying."));

        missions.Add(new Mission(
            "Nuclear Winter",
            "This spy have the coordinates to a nuclear bomb that send all South America into a nuclear winter for 30 years.",
            "Good job! South America will be safe and warm for the next 30 years… ok not that safe…",
            "Oh no! The nuclear winter destroyed a big forest and the whole world have an oxygen problem now… no o-one… c-can.. b-bre-ath…"));

        missions.Add(new Mission(
            "Game of Spoilers",
            "This spy have all the GoT 8th season’s spoilers to spread worldwide.",
            "Yes! You did well and killed that bastard!!!",
            "Oh no! All the spoilers are online and the World War Three has began!"));

        missions.Add(new Mission(
            "560 characters ",
            "This spy have the information to change Twitter’s character limit to 560 letters.",
            "Great! You stopped people to post huge and unwanted big texts in a social media made to be short and simple.",
            "Oh no! Twitter was made to keep people in touch and spread information in a faster, easier, funnier and simple way. For years, it was safe from big and unwanted posts, making all its users very happy and comfortable about a clean timeline, full of smart and practical information. Now Twitter have a lot of stories that start with “I’m not really into make texts this big, but I think it’s very funny that…”, useless posts about weather, family parties, good morning and good fucking night all the time, and #hastagsarealldestroyedbypeoplewhodontknowhowtouseit"));

        missions.Add(new Mission(
            "Pls come to Brazil",
            "This spy have the telephone number from all the artists that refuses to visit Brazil to spread worldwide.",
            "Well… not that well done, because we want to know who would betray brazillian fans like that, but whatever.",
            "Oh no! Now brazilian fans are calling and sending texts messages to the artists!"));

        missions.Add(new Mission(
            "K-pop Idol",
            "This spy have a scandal from a huge kpop boyband to spread it worldwide.",
            "Great! You stopped the (fake, of course) information, and will enjoy a new album soon!",
            "Oh no! This boyband’s fans are boycotting them, which is downing South Korea’s economy fast!"));

        missions.Add(new Mission(
            "Coffee Shop",
            "This spy have the world declaration that forbids caffeine forever",
            "Great! Caffeine is safe, caffeine is life, caffeine is happiness, caffeine is everything!!!",
            "Oh no! People are getting into accidents from sleeping in the street!"));

        missions.Add(new Mission(
            "Multiscreen",
            "This spy have the liberation to use smartphones to watching videos while walking around the street.",
            "Good job! You prevented that even more stupid people would this terrible act against society.",
            "Oh no! People are getting into accidents for bumping into each-other."));
    }
}
