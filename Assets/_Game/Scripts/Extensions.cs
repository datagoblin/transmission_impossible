﻿using System;
using System.Collections;
using UnityEngine;

public static class MonobehaviourExtensions {

	public static Coroutine RunDelayed(this MonoBehaviour mb, Action action, float seconds)
    {
        return mb.StartCoroutine(RunAfter(action, seconds));
    }

    private static IEnumerator RunAfter(Action callback, float seconds)
    {
        yield return new WaitForSeconds(seconds);
        callback();
    }
}
