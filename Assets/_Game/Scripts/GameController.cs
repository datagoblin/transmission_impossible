﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Globalization;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{

    public static readonly string SCENE_NAME = "Game";
    public Level LevelPrefab;

    [Header("Props")]
    public LayerMask boosttHitLayer;
    public Prop propPrefab;

    [Header("Booth")]
    public Booth boothPrefab;

    [Header("Actors")]
    public Actor ActorPrefab;
    public int population = 20;

    [Header("Shot")]
    public SpyActor spyActorPrefab;

    [Header("Shot")]
    public Image crosshair;
    public bool aiming = false;
    public LayerMask shotHitLayer;

    [Header("Agents")]
    public Text agentCountText;
    public GameObject buttonsRoot;
    public int agentCount = 2;
    public Agent agentPrefab;
    public List<Sprite> agentSprites;

    [Header("Cooldown")]
    public CanvasGroup cooldownHolder;
    public Text cooldownText;
    public int cooldownMaxValue;
    public int cooldownDelaySeconds;

    [Header("Trasnmission Bar")]
    public Image transmissionImage;
    public Sprite[] transmissionSprites;
    public Text transmissionText;

    [Header("Casualties Bar")]
    public Image[] casualtiesSkulls;
    public Sprite redSkullSprite;
    public Sprite greenSkullSprite;

    [Header("Dossie")]
    public Dossie dossie;


    public Image overlay;


    [Header("Audio BGM")]
    public AudioClip bgm_intro;
    public AudioClip bgm_Ambience;
    public AudioClip bgm1_loopClip;
    public AudioClip bgm2_loopClip;
    public AudioClip bgm3_loopClip;
    public AudioClip bgm4_loopClip;
    public AudioClip bgm5_loopClip;
    public AudioClip victorySting;
    public AudioClip failSting;


    [Header("Audio SFX")]
    public AudioClip sfxShoot;
    public AudioClip sfxCheck;
    public AudioClip sfxCheck2;
    public AudioClip sfxdenied;
    public AudioClip sfxselect;
    public AudioClip sfxtime;
    public AudioClip sfxTransmission;
    public AudioClip stampClip;

    [Header("Mission Modal")]
    public RectTransform MissionPanelRect;
    public Image MissionBGImage;
    public Text MissionSpyNameText;
    public Text MissionPacketText;
    public Text MissionCasualetiesText;
    public Text MissionDescriptionText;
    public Text MissionTitleText;
    public Text MissionResultText;
    public Image stampImage;
    public CanvasGroup MissionModalCanvasGroup;

    [Header("Stamps")]
    public Sprite victoryStamp;
    public Sprite defeatStamp;

    public bool hasGameOver = false;


    public Bounds bounds;

    private Spy currentSpy;

    private List<Transform> propPositions;
    private Level level;
    private bool agentsOpened = false;
    private bool placingAgent = false;
    private List<PropInfo> propInfos;

    public int deathCount = 0;
    public List<Transform> boothPositions;
    public int packetsSent = 0;

    public static void Show()
    {
        SceneManager.LoadScene(SCENE_NAME);
    }

    IEnumerator PlayAudios()
    {
        AudioManager.Instance.BGMSources.ForEach(s => s.Pause());

        AudioManager.Instance.BGMSources[0].PlayClip(bgm_intro);
        AudioManager.Instance.BGMSources[5].PlayClip(bgm_Ambience, true, 0.1f);
        yield return new WaitUntil(() => !AudioManager.Instance.BGMSources[0].isPlaying);
        AudioManager.Instance.BGMSources[0].PlayClip(bgm1_loopClip, true, 0.70f);
        AudioManager.Instance.BGMSources[1].PlayClip(bgm2_loopClip, true, 0);
        AudioManager.Instance.BGMSources[2].PlayClip(bgm3_loopClip, true, 0);
        AudioManager.Instance.BGMSources[3].PlayClip(bgm4_loopClip, true, 0);
        AudioManager.Instance.BGMSources[4].PlayClip(bgm5_loopClip, true, 0);
    }
    private void Awake()
    {
        hasGameOver = false;
    }

    void Start()
    {
        overlay.color = Color.black;
        overlay.DOFade(0, 1);
        StartCoroutine("PlayAudios");
        OnClickSpy();
        deathCount = packetsSent = 0;
        level = Instantiate(LevelPrefab);
        bounds = new Bounds(Vector3.zero, Vector3.one);

        crosshair.gameObject.SetActive(false);

        propPositions = new List<Transform>(level.propsPositions);
        propInfos = new List<PropInfo>(Resources.LoadAll<PropInfo>("PropInfos"));

        propPositions.ForEach((t) =>
        {
            var info = propInfos[Random.Range(0, propInfos.Count)];
            Prop p = Instantiate(propPrefab);
            p.Init(info);
            propInfos.Remove(info);
            p.transform.position = t.position;
            p.transform.SetParent(level.transform);
        });

        boothPositions = new List<Transform>();

        level.boothsPositions.ForEach((t) =>
        {
            Booth b = Instantiate(boothPrefab);
            b.transform.position = t.position;
            b.transform.SetParent(level.transform);
            boothPositions.Add(b.transform);
        });

        bounds.center = new Vector3(0, -.65f, 0);
        bounds.size = new Vector3(6 * 2, 4.5f * 2, .5f * 2);

        currentSpy = new Spy();
        dossie.Init(currentSpy);
        var spy = Instantiate<SpyActor>(spyActorPrefab);
        spy.transform.SetParent(level.transform);
        spy.Init(this, currentSpy);

        for (int i = 0; i < population; i++)
        {
            var actor = Instantiate<Actor>(ActorPrefab);
            actor.Init(this, currentSpy);
            actor.transform.position = Random.insideUnitCircle * 4;
            actor.transform.SetParent(level.transform);
        }

        agentCountText.text = "x" + agentCount.ToString();

        buttonsRoot.SetActive(false);
        transmissionImage.sprite = transmissionSprites[0];

    }

    void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(bounds.center, bounds.size);
    }

    private void Update()
    {
        if (aiming)
        {
            crosshair.transform.position = Input.mousePosition;
            if (Input.GetMouseButtonDown(0))
            {
                Shoot();
            }
        }
        else if (placingAgent)
        {
            crosshair.transform.position = Input.mousePosition;
            if (Input.GetMouseButtonUp(0) && Camera.main.ScreenToViewportPoint(Input.mousePosition).x > 0.2f)
            {
                var agent = Instantiate(agentPrefab);
                agent.transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                agent.transform.position = new Vector3(agent.transform.position.x, agent.transform.position.y, 5);
                agent.transform.SetParent(level.transform);
                crosshair.gameObject.SetActive(false);
                placingAgent = false;
            }
        }
        else
        {
            if (Input.GetMouseButtonDown(0))
            {
                OnClick(false);
            }
            if (Input.GetMouseButtonDown(1))
            {
                OnClick(true);
            }
        }


        Cursor.visible = !aiming;
    }

    public void GameOver(bool win)
    {
        MissionResultText.text = win ? "YOU CAUGHT THE SPY!" : "YOU FAILED";
        MissionDescriptionText.text = win ? currentSpy.mission.success : currentSpy.mission.fail;
        MissionTitleText.text = currentSpy.mission.title.ToUpper();
        stampImage.color = Color.clear;
        stampImage.sprite = win ? victoryStamp : defeatStamp;
        var op = MissionPanelRect.position;
        this.RunDelayed(() =>
        {
            AudioManager.Instance.PlaySFX(stampClip, 2);
        }, 3.5f);
        MissionPanelRect.DOShakePosition(0.3f, 20f, 30).SetDelay(3).OnComplete(() =>
         {
             MissionPanelRect.position = op;
             stampImage.color = Color.white;
         });
        aiming = false;
        crosshair.gameObject.SetActive(false);
        hasGameOver = true;

        StopCoroutine("PlayAudios");
        AudioManager.Instance.BGMSources.ForEach(s => s.Pause());
        AudioClip clip = win ? victorySting : failSting;
        AudioManager.Instance.PlaySFX(clip);
        this.RunDelayed(() => ShowEndMissionPanel(), 2);
    }

    public void Shoot()
    {

        AudioManager.Instance.PlaySFX(sfxShoot);
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 100.0f, shotHitLayer))
        {
            var hitGameObject = hit.transform.gameObject;
            Actor actor = hitGameObject.GetComponent<Actor>();
            if (actor != null)
            {
                actor.Die();
                return;
            }

            Booth booth = hitGameObject.GetComponent<Booth>();
            if (booth != null)
            {
                booth.GetShot();
                return;
            }
        }
    }

    public void AudioIntensifier()
    {
        var i = Mathf.Clamp((deathCount + packetsSent), 0, 4);
        AudioManager.Instance.BGMSources[i].DOFade(0.7f, 1);
    }

    private void OnClick(bool isRightClick)
    {
        //Debug.Log("OnClick: " + (isRightClick ? "RIGHT" : "LEFT"));
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 100.0f, shotHitLayer))
        {
            //Debug.Log("Clicked on actor!");
            var suspicion = isRightClick ? Actor.Suspicion.IsBadGuy : Actor.Suspicion.IsGoodGuy;
            SetSuspectActor(hit, ray, suspicion);
        }
        else
        if (Physics.Raycast(ray, out hit, 100.0f, boosttHitLayer))
        {
            if (isRightClick)
                return;

            Debug.Log("Clicked on prop!");
            BoostProp(hit, ray);
        }
    }

    public void SetSuspectActor(RaycastHit hit, Ray ray, Actor.Suspicion suspicion)
    {
        if (Physics.Raycast(ray, out hit, 100.0f, shotHitLayer))
        {
            Actor actor = hit.transform.gameObject.GetComponent<Actor>();
            if (actor != null)
                actor.SetSuspectLabel(suspicion);
        }
    }

    public void BoostProp(RaycastHit hit, Ray ray)
    {
        if (!cooldownText.text.Equals("0"))
        {
            AudioManager.Instance.PlaySFX(sfxdenied);
            return;
        }

        if (Physics.Raycast(ray, out hit, 100.0f, boosttHitLayer))
        {
            cooldownText.text = cooldownMaxValue.ToString();
            cooldownHolder.DOFade(1, .2f);

            DOTween.To(
                () => float.Parse(cooldownText.text, CultureInfo.InvariantCulture.NumberFormat),
                value => cooldownText.text = Mathf.FloorToInt(value).ToString(),
                0,
                cooldownDelaySeconds
            ).SetEase(Ease.Linear).OnComplete(() => { cooldownHolder.DOFade(0, .2f); AudioManager.Instance.PlaySFX(sfxtime); });

            Prop prop = hit.transform.gameObject.GetComponent<Prop>();
            if (prop != null)
                prop.OnBoost();
        }
    }

    public void UpdateTransmission()
    {
        var t = ((float)packetsSent / 4);

        float current = (float)packetsSent / 4;

        if (packetsSent >= 4) GameOver(false);
        DOTween.To(() => current, x => transmissionText.text = ((int)(x * 100)).ToString() + "%", t, 1);
        transmissionImage.sprite = transmissionSprites[packetsSent];
        AudioManager.Instance.PlaySFX(sfxTransmission);
    }

    public void UpdateCasualtiesPanel()
    {
        casualtiesSkulls[deathCount - 1].sprite = redSkullSprite;
    }

    public void OnClickSpy()
    {
        dossie.Show();

        AudioManager.Instance.PlaySFX(sfxCheck);
    }

    public void OnClickQuit()
    {
        Application.Quit();
        AudioManager.Instance.PlaySFX(sfxCheck);
    }

    public void OnClickAgents()
    {
        AudioManager.Instance.PlaySFX(sfxCheck);
        if (!agentsOpened)
        {
            if (agentCount > 0)
            {
                buttonsRoot.SetActive(true);
                agentsOpened = true;
            }
        }
        else
        {
            buttonsRoot.SetActive(false);
            agentsOpened = false;
        }
    }
    public void OnClickShot()
    {
        AudioManager.Instance.PlaySFX(sfxCheck);
        aiming = !aiming;
        Cursor.visible = !aiming;
        crosshair.gameObject.SetActive(aiming);
    }

    public void OnClickAgent(int type)
    {
        AudioManager.Instance.PlaySFX(sfxCheck);
        crosshair.gameObject.SetActive(true);
        crosshair.sprite = agentSprites[type];
        placingAgent = true;
        switch (type)
        {
            case 0:
                Debug.Log("Agent Bunny");
                break;
            case 1:
                Debug.Log("Agent Funny");
                break;
            case 2:
                Debug.Log("Agent Cunning");
                break;
            case 3:
                Debug.Log("Agent Humming");
                break;
            case 4:
                Debug.Log("Agent Mumbling");
                break;

        }
        agentCount--;
        agentCountText.text = "x" + agentCount.ToString();
        buttonsRoot.SetActive(false);
        agentsOpened = false;
    }

    public void OnClickMainMenu()
    {
        //LoadingController.Show(MainController.SCENE_NAME, GameController.SCENE_NAME);
        overlay.DOFade(1, 1).OnComplete(() =>
        {
            this.RunDelayed(() => MainController.Show(), 1);

        });
        AudioManager.Instance.PlaySFX(sfxCheck);
    }


    public void OnClickReload()
    {
        //LoadingController.Show(GameController.SCENE_NAME, GameController.SCENE_NAME);
        overlay.DOFade(1, 1).OnComplete(() =>
        {
            this.RunDelayed(() => GameController.Show(), 1);

        });
        AudioManager.Instance.PlaySFX(sfxCheck);
    }

    public void ShowEndMissionPanel()
    {
        MissionPanelRect.transform.parent.gameObject.SetActive(true);
        MissionCasualetiesText.text = deathCount.ToString();
        Debug.Log("packetsSent " + packetsSent);
        MissionPacketText.text = (packetsSent / 4f * 100f).ToString() + "%";
        Debug.Log("packetsSent %" + (packetsSent / 4f * 100f).ToString() + "%");
        MissionSpyNameText.text = currentSpy.fullName;
        MissionBGImage.DOFade(0.5f, 0.2f).OnComplete(() => MissionPanelRect.DOAnchorPosY(-34, 0.3f).SetEase(Ease.InOutQuad));
    }

    public void OnClickCloseEndMissionPanel()
    {
        AudioManager.Instance.PlaySFX(sfxCheck);
        MissionPanelRect.DOAnchorPosY(-1200f, 0.3f).SetEase(Ease.InOutQuad).OnComplete(() =>
        {
            MissionModalCanvasGroup.DOFade(0, 0.2f);
            OnClickReload();
        });
    }

}
