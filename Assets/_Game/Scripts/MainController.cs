﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainController : MonoBehaviour
{
    public static readonly string SCENE_NAME = "MainScreen";

    public AudioClip bgm_intro;
    public AudioClip bgm_loop;
    public Image overlay;

    [Header("Audio SFX")]
    public AudioClip sfxCheck;
    public AudioSource source;

    public static void Show()
    {
        SceneManager.LoadScene(SCENE_NAME);
    }

    IEnumerator Start()
    {
        overlay.color = Color.black;
        overlay.DOFade(0, 1);
        source.PlayClip(bgm_intro, false);
        yield return new WaitUntil(() => !source.isPlaying);
        source.PlayClip(bgm_loop, true);
    }

    public void OnClickStartGame()
    {
        source.DOFade(0, 1);
        overlay.DOFade(1, 1).OnComplete(() =>
        {
            this.RunDelayed(() => GameController.Show(), 1);

        });
        //AudioManager.Instance.PlaySFX(sfxCheck);
    }

    public void OnClickCredits()
    {
        Debug.Log("OnClickCredits");
        AudioManager.Instance.PlaySFX(sfxCheck);
    }

    public void OnClickQuit()
    {
        Application.Quit();
        AudioManager.Instance.PlaySFX(sfxCheck);
    }

}
