﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prop : MonoBehaviour, ICollisionListener
{
    public PropTag PropTag;

    private Rigidbody2D body2D;
    private Collider2D coll2D;
    private ColliderHelper helper;
    private SpriteRenderer spriteRenderer;

    public PropInfo info;
    public int Priority = 0;
    private AudioClip clip;
    private Coroutine ResetPriorityCoroutine;

    void Awake()
    {
        body2D = GetComponentInChildren<Rigidbody2D>();
        coll2D = GetComponentInChildren<Collider2D>();
        helper = GetComponentInChildren<ColliderHelper>();
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();

        transform.position = new Vector3(transform.position.x, transform.position.y, -1000);
    }

    public void Init(PropInfo info)
    {
        this.info = info;
        PropTag = info.propTag;
        clip = info.clip;
        spriteRenderer.GetComponent<Animator>().runtimeAnimatorController = info.animatorController;
        ((CircleCollider2D)coll2D).radius = info.effectRadius;
        spriteRenderer.sprite = info.sprite;
    }

    void Start()
    {
        helper.SetListener(this);
    }

    #region trigger events
    public void _OnTriggerEnter(Collider2D other)
    {
        //Debug.Log("OnTriggerEnter");
    }

    public void _OnTriggerExit(Collider2D other)
    {
        //Debug.Log("OnTriggerExit");
    }

    public void _OnTriggerStay(Collider2D other)
    {
        //Debug.Log("OnTriggerStay");
        var actor = other.GetComponentInParent<Actor>();
        if (actor != null)
        {
            actor.Affect(transform.position, PropTag, Priority);
        }
    }
    #endregion

    public virtual void OnBoost()
    {
        Debug.Log("BOOSTING A PROP!");
        Priority = 2;
        StartCoroutine(ResetPriority());
        AudioManager.Instance.PlaySFX(clip);
    }

    private IEnumerator ResetPriority()
    {
        yield return new WaitForSeconds(.4f);
        Priority = 0;
    }
}

public enum PropTag
{
    Meat = 0,
    Animal,
    Circus,
    SeaFood,
    Water,
    Dancing,
    Flower,
    Fire,
    Vegetables,
    Count,
    Corpse,
}
