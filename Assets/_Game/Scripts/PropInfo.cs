﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PropInfo : ScriptableObject
{
    public PropTag propTag;
    public Sprite sprite;
    public float effectRadius;
    public AudioClip clip;
    public RuntimeAnimatorController animatorController;
}