﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Spy
{
    public string fullName;
    public List<Persona> personas;
    public Mission mission;

    [Space(10)]
    public Corpse CorpsePrefab;

    public string[] names = new string[] {
        "Jinnie Jigings",
        "Vick Vespers",
        "Kenzie Firebird",
        "Montana Darkness",
        "Sam Firebird",
        "Daylen Razorblades",
        "Haven Carcrash",
        "Sam Moonstalker",
        "Halley Hollow",
        "Sable Goblinglitter",
        "Tanner Malice",
        "Matt Boner",
        "Jett Jager",
        "Nara Carbo",
        "Amy Manders",
        "Dick Vinie",
        "Nye Shuu",
        "G.Will",
        "Mor Augustin",
        "Lu An",
        "Lu Is"
    };
    static List<Persona> personasList = new List<Persona>() {
        new Persona("Circus trauma", new Dictionary<PropTag, int>() { {PropTag.Circus, -1}, {PropTag.Animal, -1} }),
        new Persona("Militant vegan", new Dictionary<PropTag, int>() { {PropTag.Animal, 1}, {PropTag.Meat, -1} }),
        new Persona("Almost drowned as a kid", new Dictionary<PropTag, int>() { {PropTag.Water,-1}, {PropTag.SeaFood, -1} }),
        new Persona("Hopeless romantic", new Dictionary<PropTag, int>() { {PropTag.Flower, 1}, {PropTag.Dancing, 1} }),
        new Persona("Peter Pan syndrome", new Dictionary<PropTag, int>() { {PropTag.Circus,1}, {PropTag.Vegetables, -1} }),
        new Persona("Little mermaid", new Dictionary<PropTag, int>() { {PropTag.Water, 1}, {PropTag.Fire, -1} }),
        new Persona("Yes today, Satan", new Dictionary<PropTag, int>() { {PropTag.Fire, 1}, {PropTag.Animal, -1} }),
        new Persona("Nero wannabe", new Dictionary<PropTag, int>() { {PropTag.Fire, 1}, {PropTag.Water, -1} }),
        new Persona("Deadliest catcher", new Dictionary<PropTag, int>() { {PropTag.Water, 1}, {PropTag.SeaFood, 1} }),
        new Persona("Born to be hippie", new Dictionary<PropTag, int>() {
            {PropTag.Meat, 1}, {PropTag.Animal, 1}, {PropTag.Circus, 1}, {PropTag.SeaFood, 1}, {PropTag.Water, 1},
            {PropTag.Dancing, 1}, {PropTag.Flower, 1}, {PropTag.Fire, 1}, {PropTag.Vegetables, 1}
        })
    };

    public Spy()
    {
        fullName = names[Random.Range(0, names.Length)];
        personas = new List<Persona>();
        Mission.Init();
        mission = Mission.missions[Random.Range(0, Mission.missions.Count)];

        var max = 3;
        while (personas.Count < 2 || max > 0)
        {
            var p = personasList[Random.Range(0, personasList.Count)];
            bool conflicted = false;
            foreach (var persona in personas)
            {
                if (p.HasConflict(persona))
                {
                    conflicted = true;
                    break;
                }
            }

            max--;
            if (!conflicted)
            {
                personas.Add(p);
            }
        }
    }

}

public class Persona
{
    public string description;
    public Dictionary<PropTag, int> affinity;

    public Persona(string description, Dictionary<PropTag, int> affinity)
    {
        this.description = description;
        this.affinity = affinity;
    }

    public bool HasConflict(Persona persona)
    {
        foreach (var k in affinity.Keys)
        {
            if (persona.affinity.ContainsKey(k))
                return true;
        }
        return false;
    }
}

