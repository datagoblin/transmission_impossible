﻿using System.Collections.Generic;
using System.Linq;
using System;

public class SpyActor : Actor
{

    public void Init(GameController controller, Spy spyInfo)
    {

        this.controller = controller;

        affinity = new Dictionary<PropTag, int>();
        foreach (var persona in spyInfo.personas)
        {
            affinity = affinity.Union(persona.affinity).ToDictionary(kv => kv.Key, kv => kv.Value);
        }


        for (int i = 0; i < (int)PropTag.Count; i++)
        {
            if (affinity.ContainsKey((PropTag)i))
                return;

            affinity.Add((PropTag)i, UnityEngine.Random.value > 0.5f ? 1 : -1);
        }

    }

    internal void SendPacket()
    {
        controller.packetsSent++;
        controller.AudioIntensifier();
        this.RunDelayed(() => controller.UpdateTransmission(), 3);
    }
}
