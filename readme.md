# Transmission Impossible

A game made in 48 hours the 2018 edition of the Global Game Jam, with the theme "transmission".

[Play it over at the GGJ site!](https://globalgamejam.org/2018/games/transmission-impossible-13)
